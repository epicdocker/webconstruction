<!-- vscode-markdown-toc -->
* 1. [Version](#Version)
* 2. [Requirements](#Requirements)
* 3. [Environments](#Environments)
* 4. [Examples](#Examples)
	* 4.1. [Default](#Default)
	* 4.2. [Custom](#Custom)
* 5. [License](#License)
	* 5.1. [ICON License](#ICONLicense)
	* 5.2. [Images License](#ImagesLicense)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

# Web-Construction 

Nginx to display a construction site page.

##  1. <a name='Version'></a>Version 

There is no versioning of this project, it will be rebuilt weekly using the current official version of [nginx](https://hub.docker.com/_/nginx/) "alpine".

If you need a different version, you may copy and modify my templates as needed.

##  2. <a name='Requirements'></a>Requirements

- Docker installed - https://www.docker.com/get-docker
- Optional: Docker Compose installed (is needed for examples) - https://docs.docker.com/compose/

##  3. <a name='Environments'></a>Environments

`WEBCONSTRUCTION_TITLE`  

*default:* epicsoft.de - Web-Construction

Title of the construction website.

`WEBCONSTRUCTION_FAVICON`

*default:* cone

Name of the favicon or absolute URL to an icon.

`WEBCONSTRUCTION_IMAGE`  

*default:* 0

Selection of the background image possible values are 0 to 9 or absolute URL to an image.

`WEBCONSTRUCTION_BACKGROUND_COLOR`

*default:* black

Use [HTML Color Names](https://www.w3schools.com/colors/colors_names.asp) or HEX Value for the background.

`WEBCONSTRUCTION_BACKGROUND_IMAGE_SIZE_IN_PERCENT`

*default:* 90

Background image size in percent possible values are 1 to 100.

##  4. <a name='Examples'></a>Examples

Start one example and open the URL `http://localhost/` in the browser.

###  4.1. <a name='Default'></a>Default

    docker run --rm -it \
      --name webconstruction \
	  -p 80:80 \
	  registry.gitlab.com/epicdocker/webconstruction

###  4.2. <a name='Custom'></a>Custom

    docker run --rm -it \
      --name webconstruction \
	  -p 80:80 \
	  -e WEBCONSTRUCTION_TITLE='My Custom Title' \
	  -e WEBCONSTRUCTION_FAVICON=house \
	  -e WEBCONSTRUCTION_IMAGE=7 \
	  -e WEBCONSTRUCTION_BACKGROUND_COLOR=WhiteSmoke \
	  -e WEBCONSTRUCTION_BACKGROUND_IMAGE_SIZE_IN_PERCENT=80 \
	  registry.gitlab.com/epicdocker/webconstruction

##  5. <a name='License'></a>License

All files created by epicsoft.de are under the MIT License see [LICENSE](https://gitlab.com/epicdocker/webconstruction/blob/master/LICENSE)

###  5.1. <a name='ICONLicense'></a>ICON License

Free for commercial use 

- cone.ico - https://www.iconfinder.com/icons/512542/cone_construction_maintenance_traffic_icon
- gear.ico - https://www.iconfinder.com/icons/1886783/cog_cogwheel_gear_options_repr_setting_icon
- hammer.ico - https://www.iconfinder.com/icons/1250320/construction_hammer_options_settings_icon
- house.ico - https://www.iconfinder.com/icons/384890/building_home_house_icon

###  5.2. <a name='ImagesLicense'></a>Images License

CC0 Creative Commons see https://creativecommons.org/publicdomain/zero/1.0/

- 0.jpg - https://pixabay.com/de/im-bau-baustelle-bauen-arbeit-2891888/
- 1.png - https://pixabay.com/de/wartung-under-construction-im-bau-2422173/
- 2.jpg - https://pixabay.com/de/bagger-schaufelbagger-catapillar-51665/
- 3.jpg - https://pixabay.com/de/zukunft-buchstaben-baustelle-leiter-3274954/
- 4.jpg - https://pixabay.com/de/bauland-baustelle-bauhof-3391379/
- 5.jpg - https://pixabay.com/de/hausbau-handwerker-baustelle-3102324/
- 6.jpg - https://pixabay.com/de/wartung-under-construction-im-bau-2422172/
- 7.png - https://pixabay.com/de/im-bau-wohnflaeche-vertrag-website-150271/
- 8.jpg - https://pixabay.com/de/maurer-bauarbeiter-hausbau-1019810/
- 9.jpg - https://pixabay.com/de/verkehrszeichen-baustelle-achtung-663360/
