#### Mo - Mustache Templates in Bash - https://github.com/tests-always-included/mo
FROM alpine:latest AS mo_buildenv

ENV MO_VERSION="2.0.4"
ENV MO_FILENAME="${MO_VERSION}.tar.gz"
ENV MO_DOWNLOAD_URL="https://github.com/tests-always-included/mo/archive/${MO_FILENAME}" \
    MO_DOWNLOAD_CHECKSUM="8fdf55b0489a9eb82163a53a8391a0114a774a21a28f3e0d06d6aa4fdcf6d3b11847c5ad90f265ca3d270929ff190df03595ab6c422a0b69f8080f7988755b7c"

RUN cd / \
 && wget ${MO_DOWNLOAD_URL} \
 && sha512sum /${MO_FILENAME} \
 && echo "${MO_DOWNLOAD_CHECKSUM}  /${MO_FILENAME}" | sha512sum -c - \
 && tar -xf ${MO_FILENAME} -C /tmp \
 && mv /tmp/mo-${MO_VERSION}/mo /mo \
 && chmod +x /mo

FROM nginx:alpine

ARG BUILD_DATE
LABEL org.label-schema.name="Webconstruction" \
      org.label-schema.description="Nginx to display a construction site page" \
      org.label-schema.vendor="epicsoft.de / Alexander Schwarz <schwarz@epicsoft.de>" \
      org.label-schema.version="latest" \
      org.label-schema.schema-version="1.0" \
      org.label-schema.build-date=${BUILD_DATE}

LABEL image.name="epicsoft-webconstruction" \
      image.description="Nginx to display a construction site page" \
      maintainer="epicsoft.de" \
      maintainer.name="Alexander Schwarz <schwarz@epicsoft.de>" \
      maintainer.copyright="Copyright 2018-2022 epicsoft.de / Alexander Schwarz" \
      license="MIT"

ENV WEBCONSTRUCTION_TITLE="epicsoft.de - Web-Construction" \
    WEBCONSTRUCTION_FAVICON="cone" \
    WEBCONSTRUCTION_IMAGE="0" \
    WEBCONSTRUCTION_BACKGROUND_COLOR="black" \
    WEBCONSTRUCTION_BACKGROUND_IMAGE_SIZE_IN_PERCENT="90"

RUN apk --no-cache add bash

COPY [ "etc/", "etc/"]
COPY [ "usr/", "usr/"]
COPY --from=mo_buildenv [ "/mo", "/opt/mo/mo" ]

RUN chmod 550 /etc/epicsoft/*.sh \
 && chmod -R 555 /usr/share/nginx/html

ENTRYPOINT [ "/etc/epicsoft/entrypoint.sh" ]

EXPOSE 80 

CMD ["nginx", "-g", "daemon off;"]
