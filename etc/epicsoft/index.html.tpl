<!DOCTYPE html>
<html lang="en-US">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<link rel="shortcut icon" type="image/x-icon" href="{{ HTML_FAVICON }}">
<title>{{ WEBCONSTRUCTION_TITLE }}</title>
<style type="text/css">
* {
padding: 0;
margin: 0;
}
body {
width: 100vw;
height: 100vh;
background-color: {{ WEBCONSTRUCTION_BACKGROUND_COLOR }};
overflow: none;
}
.imageWrapper {
display: flex;
align-items: center;
justify-content: center;
}
.imageWrapper:before {
content: '';
display: block;
width: 0;
height: 100vh;
flex: 0 0 auto;
}
.imageWrapper div {
width: {{ WEBCONSTRUCTION_BACKGROUND_IMAGE_SIZE_IN_PERCENT }}vw;
height: {{ WEBCONSTRUCTION_BACKGROUND_IMAGE_SIZE_IN_PERCENT }}vh;
margin: 0 auto;
background-image: url('{{ HTML_BACKGROUND_IMAGE_URL }}');
-webkit-background-size: contain;
background-size: contain;
background-position: center;
background-repeat: no-repeat;
}
</style>
</head>
<body>
<div class="imageWrapper"><div></div></div>
</body>
</html>