#!/bin/bash 

echo "[INFO] $(date -Iseconds) Starting initialization"

declare HTML_FAVICON=$WEBCONSTRUCTION_FAVICON
declare HTML_BACKGROUND_IMAGE_URL=$WEBCONSTRUCTION_IMAGE

if [[ -f /usr/share/nginx/html/$WEBCONSTRUCTION_FAVICON.ico ]]; then 
  HTML_FAVICON=/$WEBCONSTRUCTION_FAVICON.ico
fi

if [[ -f /usr/share/nginx/html/$WEBCONSTRUCTION_IMAGE.jpg ]]; then 
  HTML_BACKGROUND_IMAGE_URL=/$WEBCONSTRUCTION_IMAGE.jpg
elif [[ -f /usr/share/nginx/html/$WEBCONSTRUCTION_IMAGE.png ]]; then 
  HTML_BACKGROUND_IMAGE_URL=/$WEBCONSTRUCTION_IMAGE.png
fi 

. /opt/mo/mo # This loads the "mo" function
cat /etc/epicsoft/default.conf.tpl | mo --fail-not-set > /etc/nginx/conf.d/default.conf
cat /etc/epicsoft/index.html.tpl | mo --fail-not-set > /usr/share/nginx/html/index.html

chmod 555 /usr/share/nginx/html/index.html

echo "[INFO] $(date -Iseconds) Initialization done"

exec "$@"
